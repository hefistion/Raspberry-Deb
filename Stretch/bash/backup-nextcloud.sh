#!/bin/bash

############################################################################
# Simple backup script. It uses complete and incremental backups, with     #
# hard links to simulate snapshots. $FULL_BACKUP_LIMIT controls the        #
# frecuency of full backups.It accepts at least one source directory and a #
# single destination directory as arguments. Usage:                        #
#                                                                          #
# incremental_backup.sh SOURCE_DIRECTORY_1 [SOURCE_DIRECTORY_2..N]  	     #
#       DESTINATION_DIRECTORY                                              #
# todo: check if the log file exists. rotate                               #
#                                                                          #
#                                                                          #
#  Author: Álvaro Reig González                                            #
#  Licence: GNU GLPv3                                                      #
#  www.alvaroreig.com                                                      #
#  https://github.com/alvaroreig                                           #
#                                                                          #
#  Modificado: Carlos M.                                                   #
#  https://elblogdelazaro.gitlab.io/                                       #
#                                                                          #
############################################################################

#!/bin/sh

DATE=`date +%Y%m%d%H%M%S`
DEST_DIR='/media/Nas01/Backups/nextcloud'             # Carpeta destino backup
SOURCE_DIRS='/var/www/nextcloud'                      # Carpeta origen backup
NXT_BACKUP_STRING=nextcloud-dirbkp-$DATE              # Nombre backup nextcloud
BD_BACKUP_STRING=nextcloud-sqlbkp-$DATE               # Nombre backup BD
ERROR=0                                               # Control errores

BACKUPS_TO_KEEP=7                                     # Numero de Backups a conservar
EXCLUSSIONS=" "                                       # ficheros a excluir del backup
OPTIONS="-Aax --progress"                             # -n para hacer simular rsync

############################################################################
# Buscar backups anteriores                                                #
############################################################################

BACKUPS=`ls -t $DEST_DIR |grep backup-`
BACKUP_COUNTER=0
BACKUPS_LIST=()

for x in $BACKUPS
do
    BACKUPS_LIST[$BACKUP_COUNTER]="$x"
    echo "[" `date +%Y-%m-%d_%R` "]" "backup detectado:" ${BACKUPS_LIST[$BACKUP_COUNTER]}
    let BACKUP_COUNTER=BACKUP_COUNTER+1

done


############################################################################
# Borrar backups antiguos, si es necesario                                 #
############################################################################

if [ $BACKUPS_TO_KEEP -lt ${#BACKUPS_LIST[*]} ]; then
  let BACKUPS_TO_DELETE=${#BACKUPS_LIST[*]}-$BACKUPS_TO_KEEP
  echo "[" `date +%Y-%m-%d_%R` "]" "Necesario borrar" $BACKUPS_TO_DELETE" backups" $BACKUPS_TO_DELETE

  while [ $BACKUPS_TO_DELETE -gt 0 ]; do
    BACKUP=${BACKUPS_LIST[${#BACKUPS_LIST[*]}-1]}
    unset BACKUPS_LIST[${#BACKUPS_LIST[*]}-1]
    echo "[" `date +%Y-%m-%d_%R` "]" "Backup a borrar:" $BACKUP
    rm -rf $DEST_DIR"/"$BACKUP
    if [ $? -ne 0 ]; then
      echo "[" `date +%Y-%m-%d_%R` "]" "####### Error borrando el backup #######"
    else
      echo "[" `date +%Y-%m-%d_%R` "]" "Backup correctamente borrad0"
    fi
    let BACKUPS_TO_DELETE=BACKUPS_TO_DELETE-1
  done
else
  echo "[" `date +%Y-%m-%d_%R` "]" "No es necesario borrar backups"
fi

##########################################################################################
#  Backup de la carpeta Nextcloud                                                        #
##########################################################################################

rsync $OPTIONS  $EXCLUSSIONS $SOURCE_DIRS $DEST_DIR/$NXT_BACKUP_STRING 2>> $DEST_DIR/error.log

if [ $? -ne 0 ]; then
    echo "####### Error rsync  #######"$'\r' >> $DEST_DIR/error.log
    echo -e "rsync fallo el $(date +'%d-%m-%Y %H:%M:%S')"$'\r' >> $DEST_DIR/error.log
    echo "[" `date +%Y-%m-%d_%R` "]" "####### Error rsync  #######"
    ERROR=1
else
    echo  "[" `date +%Y-%m-%d_%R` "]" "Backup carpeta Nextcloud se realizo correctamente" >> $DEST_DIR/nextcloud-backup.log
    echo  "[" `date +%Y-%m-%d_%R` "]" "Backup carpeta Nextcloud se realizo correctamente"
fi


##########################################################################################
# Backup BD Nextcloudd                                                                   #
##########################################################################################

mysqldump --single-transaction -h localhost -u nextcloud -p'contraseña' nextcloud > $DEST_DIR/$BD_BACKUP_STRING 2>> $DEST_DIR/error.log

if [ $? -ne 0 ]; then
    echo -e "mysqldump fallo el $(date +'%d-%m-%Y %H:%M:%S')"$'\r' >> $DEST_DIR/error.log
    echo "[" `date +%Y-%m-%d_%R` "]" "####### Error mysqldump  #######"
    echo "####### Error mysqldump  #######"$'\r' >> $DEST_DIR/error.log
    ERROR=1
else
    echo  "[" `date +%Y-%m-%d_%R` "]" "Backup BD se realizo correctamente" >> $DEST_DIR/nextcloud-backup.log
    echo  "[" `date +%Y-%m-%d_%R` "]" "Backup BD se realizo correctamente"
fi

##########################################################################################
# Si no hay Error, Uno, Comprimo y Borro los Backups                                   #
##########################################################################################

if [ $ERROR -eq 0 ]; then
    tar -cvzf $DEST_DIR/backup-nextcloud-$DATE.tar.gz  $DEST_DIR/nextcloud*$DATE 2>> $DEST_DIR/error.log
    if [ $? -ne 0 ]; then
        echo "####### Error rsync  #######"$'\r' >> $DEST_DIR/error.log
        echo -e "tar -cvzf fallo el $(date +'%d-%m-%Y %H:%M:%S')"$'\r' >> $DEST_DIR/error.log
        echo "[" `date +%Y-%m-%d_%R` "]" "####### Error en la ejecucion de tar  #######"
    else
        echo  "[" `date +%Y-%m-%d_%R` "]" "tar -cvzf se realizo correctamente" >> $DEST_DIR/nextcloud-backup.log
        echo  "[" `date +%Y-%m-%d_%R` "]" "tar -cvzf se realizo correctamente"
        rm -rf $DEST_DIR/$BD_BACKUP_STRING
        rm -rf $DEST_DIR/$NXT_BACKUP_STRING
    fi
fi
